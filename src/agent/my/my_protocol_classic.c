#include "my_protocol_classic.h"
#include "my_com_data.h"

static inline char *strend(char *s)
{
	while (*s++)
		;
	return s - 1;
}

bool parse_packet(uchar *input_raw_packet, int input_packet_length,
		  struct msg *r, enum enum_server_command cmd)
{
	union COM_DATA *data = &(r->data);
	log_debug("input_raw_packet:%p, input_packet_length:%d, cmd:%d",
		  input_raw_packet, input_packet_length, cmd);
	switch (cmd) {
	case COM_QUERY: {
		int start_offset, end_offset;

		uint8_t *p = input_raw_packet;
		log_debug("len: %d", input_packet_length);

		if (*p == 0x0) {
			log_debug("len: %d", input_packet_length);
			p++;
			input_packet_length--;
		}

		if (*p == 0x1) {
			log_debug("len: %d", input_packet_length);
			p++;
			input_packet_length--;
		}

		log_debug("len: %d", input_packet_length);

		int ret = my_get_route_key(p, input_packet_length,
					   &start_offset, &end_offset);
		//if(layer <= 0 || layer > 3)
		//	layer = 3;

		if (ret < 0) {
			log_error("my_get_route_key return value:%d", ret);
			r->keys[0].start = NULL;
			r->keys[0].end = NULL;
			return false;
		} else if (ret == 1) {
			log_debug("CMD_SQL_PASS_OK");
			r->admin = CMD_SQL_PASS_OK;
			return true;
		} else if (ret == 2) {
			r->admin = CMD_SQL_PASS_NULL;
			return true;
		}

		log_debug("my_get_route_key parse success. %d, %d",
			  start_offset, end_offset);
		r->keys[0].start = input_raw_packet + start_offset;
		r->keys[0].end = input_raw_packet + end_offset;
		r->admin = CMD_NOP;
		break;
	}
	default:
		break;
	}

	return true;

malformed:
	return false;
}